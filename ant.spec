%bcond_with tests
%global ant_home %{_datadir}/ant

Name:           ant
Summary:        A Java-based build tool
Version:        1.10.12
Release:        2
Epoch:          0
License:        ASL 2.0
URL:            https://ant.apache.org/
Source0:        https://archive.apache.org/dist/ant/source/apache-ant-%{version}-src.tar.bz2
Source1:        ant.asciidoc
Source2:        apache-ant-1.8.ant.conf

Patch0001:      backport-Fix-integer-overflow-when-parsing-SOURCE_DATE_EPOCH.patch

BuildRequires:  javapackages-local java-1.8.0-devel ant >= 1.10.2
BuildRequires:  ant-junit xmlto mvn(antlr:antlr) mvn(bcel:bcel)
BuildRequires:  mvn(bsf:bsf) mvn(com.jcraft:jsch) mvn(commons-logging:commons-logging-api)
BuildRequires:  mvn(commons-net:commons-net) mvn(javax.mail:mail) mvn(jdepend:jdepend)
BuildRequires:  mvn(junit:junit) mvn(log4j:log4j:1.2.13) mvn(org.tukaani:xz)
BuildRequires:  mvn(oro:oro) mvn(regexp:regexp) mvn(xalan:xalan)
BuildRequires:  mvn(xml-resolver:xml-resolver) mvn(org.hamcrest:hamcrest-core)
BuildRequires:  mvn(org.hamcrest:hamcrest-library) junit5 asciidoc

Recommends: java-1.8.0-devel
Requires:       %{name}-lib = %{epoch}:%{version}-%{release} javapackages-tools
BuildArch:      noarch

%description
Ant is a Java based build tool. In theory it is kind of like "make"
without makes wrinkles and with the full portability of pure java code.

%package lib
Summary:        Core part of %{name}

%description lib
Core part of Apache Ant that can be used as a library.

%package jmf
Summary:        Optional jmf tasks for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description jmf
Optional jmf tasks for %{name}.


%package swing
Summary:        Optional swing tasks for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description swing
Optional swing tasks for %{name}.


%package antlr
Summary:        Optional antlr tasks for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description antlr
Optional antlr tasks for %{name}.


%package apache-bsf
Summary:        Optional apache bsf tasks for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description apache-bsf
Optional apache bsf tasks for %{name}.


%package apache-resolver
Summary:        Optional apache resolver tasks for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description apache-resolver
Optional apache resolver tasks for %{name}.


%package commons-logging
Summary:        Optional commons logging tasks for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description commons-logging
Optional commons logging tasks for %{name}.


%package commons-net
Summary:        Optional commons net tasks for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description commons-net
Optional commons net tasks for %{name}.

%package apache-bcel
Summary:        Optional apache bcel tasks for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description apache-bcel
Optional apache bcel tasks for %{name}.


%package apache-log4j
Summary:        Optional apache log4j tasks for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description apache-log4j
Optional apache log4j tasks for %{name}.


%package apache-oro
Summary:        Optional apache oro tasks for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description apache-oro
Optional apache oro tasks for %{name}.


%package apache-regexp
Summary:        Optional apache regexp tasks for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description apache-regexp
Optional apache regexp tasks for %{name}.


%package apache-xalan2
Summary:        Optional apache xalan2 tasks for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description apache-xalan2
Optional apache xalan2 tasks for %{name}.

%package imageio
Summary:Optional imageio tasks for %{name}
Requires:%{name} = %{epoch}:%{version}-%{release}

%description imageio
Optional imageio tasks for %{name}.

%package javamail
Summary:        Optional javamail tasks for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description javamail
Optional javamail tasks for %{name}.


%package jdepend
Summary:        Optional jdepend tasks for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description jdepend
Optional jdepend tasks for %{name}.

%package jsch
Summary:        Optional jsch tasks for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description jsch
Optional jsch tasks for %{name}.


%package junit
Summary:        Optional junit tasks for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description junit
Optional junit tasks for %{name}.


%package junit5
Summary:        Optional junit5 tasks for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description junit5
Optional junit5 tasks for %{name}.


%package testutil
Summary:        Test utility classes for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description testutil
Test utility tasks for %{name}.

%package xz
Summary:        Optional xz tasks for %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description xz
Optional xz tasks for %{name}.

%package        help
Summary:        Documents for %{name}
Buildarch:      noarch
Requires:       info
Obsoletes: %{name}-manual < %{epoch}:%{version}-%{release}
Obsoletes: %{name}-javadoc < %{epoch}:%{version}-%{release}
Provides: %{name}-manual = %{epoch}:%{version}-%{release}
Provides: %{name}-javadoc = %{epoch}:%{version}-%{release}

%description help
Man pages and other related documents for %{name}.

%prep
%autosetup -p1 -n apache-%{name}-%{version}

%pom_xpath_remove 'attribute[@name="Class-Path"]' build.xml
find . -name "*.jar" | xargs -t rm
pushd src/tests/junit/org/apache/tools/%{name}
rm types/selectors/SignedSelectorTest.java
rm taskdefs/condition/IsFileSelectedTest.java
rm taskdefs/condition/IsSignedTest.java
rm taskdefs/JarTest.java
popd
pushd src/tests/junit/org/apache/tools/mail
rm MailMessageTest.java
popd
build-jar-repository -s -p lib/optional antlr bcel javamail/mailapi jdepend junit log4j-1 oro regexp bsf commons-logging commons-net jsch xalan-j2 xml-commons-resolver xalan-j2-serializer hamcrest/core hamcrest/library xz-java
build-jar-repository -s -p lib/optional junit5 opentest4j
cp -p %{SOURCE2} %{name}.conf
sed -e 's:/etc/%{name}.conf:%{_sysconfdir}/%{name}.conf:g' \
    -e 's:/etc/%{name}.d:%{_sysconfdir}/%{name}.d:g' \
    -e 's:/usr/share/%{name}:%{_datadir}/%{name}:g' \
    -e 's:/usr/bin/build-classpath:%{_bindir}/build-classpath:g' \
    -e 's:/usr/share/java-utils/java-functions:%{_javadir}-utils/java-functions:g' \
    -i src/script/%{name} %{name}.conf
sed -i 's/jaxp_parser_impl//;s/xml-commons-apis//' src/script/%{name}
iconv KEYS -f iso-8859-1 -t utf-8 -o KEYS.utf8
mv KEYS.utf8 KEYS
iconv LICENSE -f iso-8859-1 -t utf-8 -o LICENSE.utf8
ln -sf LICENSE.utf8 LICENSE
%pom_xpath_remove pom:optional src/etc/poms/%{name}-antlr/pom.xml
%pom_xpath_inject 'target[@name="javadocs"]/javadoc/packageset' '<exclude name="**/junitlauncher"/>' build.xml

%pom_change_dep -r com.sun.mail:jakarta.mail javax.mail:mail src/etc/poms/ant-javamail/pom.xml

%build
%{ant} jars test-jar
%{ant} javadocs
rm -fr build/lib/%{name}-jai.jar build/lib/%{name}-netrexx.jar

mkdir man
asciidoc -b docbook -d manpage -o man/%{name}.xml %{SOURCE1}

%install
install -d %{buildroot}%{_datadir}/%{name}/{lib,etc,bin}
%mvn_alias :%{name} org.apache.%{name}:%{name}-nodeps apache:%{name} %{name}:%{name}
%mvn_alias :%{name}-launcher %{name}:%{name}-launcher
%mvn_file ':{%{name},%{name}-bootstrap,%{name}-launcher}' %{name}/@1 @1
for jar in build/lib/*.jar
do
  jar tf ${jar} | egrep -q *.class
  jarname=$(basename $jar .jar)
  ln -sf ../../java/%{name}/${jarname}.jar %{buildroot}%{_datadir}/%{name}/lib/${jarname}.jar
  pom=src/etc/poms/${jarname}/pom.xml
  [ $jarname == %{name}-bootstrap ] && pom='org.apache.%{name}:%{name}-bootstrap:%{version}'
  %mvn_artifact ${pom} ${jar}
done
%mvn_artifact src/etc/poms/pom.xml
%mvn_package :%{name} lib
%mvn_package :%{name}-launcher lib
%mvn_package :%{name}-bootstrap lib
%mvn_package :%{name}-parent lib
%mvn_package :%{name}-junit4 junit
%mvn_package ':%{name}-{*}' @1
%mvn_install
rm -f src/script/*.bat
rm -f src/script/*.cmd
cp -p src/etc/*.xsl %{buildroot}%{_datadir}/%{name}/etc
install -d %{buildroot}%{_bindir}
cp -p src/script/%{name} %{buildroot}%{_bindir}/
cp -a %{_bindir}/%{name} %{buildroot}%{_datadir}/%{name}/bin/
cp -p src/script/%{name}Run %{buildroot}%{_datadir}/%{name}/bin/
install -d %{buildroot}%{_sysconfdir}
cp -p %{name}.conf %{buildroot}%{_sysconfdir}/%{name}.conf
install -d %{buildroot}%{_sysconfdir}/%{name}.d
echo "%{name}/%{name}-jmf" > %{buildroot}%{_sysconfdir}/%{name}.d/jmf
echo "%{name}/%{name}-swing" > %{buildroot}%{_sysconfdir}/%{name}.d/swing
echo "antlr %{name}/%{name}-antlr" > %{buildroot}%{_sysconfdir}/%{name}.d/antlr
echo "rhino bsf %{name}/%{name}-apache-bsf" > %{buildroot}%{_sysconfdir}/%{name}.d/apache-bsf
echo "xml-commons-resolver %{name}/%{name}-apache-resolver" > %{buildroot}%{_sysconfdir}/%{name}.d/apache-resolver
echo "apache-commons-logging %{name}/%{name}-commons-logging" > %{buildroot}%{_sysconfdir}/%{name}.d/commons-logging
echo "apache-commons-net %{name}/%{name}-commons-net" > %{buildroot}%{_sysconfdir}/%{name}.d/commons-net
echo "bcel %{name}/%{name}-apache-bcel" > %{buildroot}%{_sysconfdir}/%{name}.d/apache-bcel
echo "log4j12 %{name}/%{name}-apache-log4j" > %{buildroot}%{_sysconfdir}/%{name}.d/apache-log4j
echo "oro %{name}/%{name}-apache-oro" > %{buildroot}%{_sysconfdir}/%{name}.d/apache-oro
echo "regexp %{name}/%{name}-apache-regexp" > %{buildroot}%{_sysconfdir}/%{name}.d/apache-regexp
echo "xalan-j2 xalan-j2-serializer %{name}/%{name}-apache-xalan2" > %{buildroot}%{_sysconfdir}/%{name}.d/apache-xalan2
echo "ant/ant-imageio" > $RPM_BUILD_ROOT%{_sysconfdir}/%{name}.d/imageio
echo "javamail jaf %{name}/%{name}-javamail" > %{buildroot}%{_sysconfdir}/%{name}.d/javamail
echo "jdepend %{name}/%{name}-jdepend" > %{buildroot}%{_sysconfdir}/%{name}.d/jdepend
echo "jsch %{name}/%{name}-jsch" > %{buildroot}%{_sysconfdir}/%{name}.d/jsch
echo "junit hamcrest/core %{name}/%{name}-junit" > %{buildroot}%{_sysconfdir}/%{name}.d/junit
echo "junit hamcrest/core %{name}/%{name}-junit4" > %{buildroot}%{_sysconfdir}/%{name}.d/junit4
echo "testutil %{name}/%{name}-testutil" > %{buildroot}%{_sysconfdir}/%{name}.d/testutil
echo "xz-java %{name}/%{name}-xz" > %{buildroot}%{_sysconfdir}/%{name}.d/xz
echo "junit5 hamcrest/core junit opentest4j %{name}/%{name}-junitlauncher" > %{buildroot}%{_sysconfdir}/%{name}.d/junitlauncher
install -d %{buildroot}%{_javadocdir}/%{name}
cp -pr build/javadocs/* %{buildroot}%{_javadocdir}/%{name}
(cd manual; ln -sf %{_javadocdir}/%{name} api)

#manpage
install -d -m 755 %{buildroot}%{_mandir}/man1/
install -p -m 644 man/%{name}.xml %{buildroot}%{_mandir}/man1/%{name}.xml

%if %with tests
%check
LC_ALL=en_US.utf8 %{ant} test
%endif

%files
%license LICENSE NOTICE
%config(noreplace) %{_sysconfdir}/%{name}.conf
%attr(0755,root,root) %{_bindir}/ant
%dir %{ant_home}/bin
%{ant_home}/bin/ant
%attr(0755,root,root) %{ant_home}/bin/antRun
%{_mandir}/man1/%{name}.*
%dir %{ant_home}/etc
%{ant_home}/etc/ant-update.xsl
%{ant_home}/etc/changelog.xsl
%{ant_home}/etc/coverage-frames.xsl
%{ant_home}/etc/mmetrics-frames.xsl
%{ant_home}/etc/log.xsl
%{ant_home}/etc/tagdiff.xsl
%{ant_home}/etc/common2master.xsl
%{ant_home}/etc/printFailingTests.xsl
%dir %{_sysconfdir}/%{name}.d

%files lib -f .mfiles-lib
%dir %{ant_home}
%dir %{ant_home}/lib
%{ant_home}/lib/%{name}.jar
%{ant_home}/lib/%{name}-launcher.jar
%{ant_home}/lib/%{name}-bootstrap.jar

%files jmf -f .mfiles-jmf
%{ant_home}/lib/%{name}-jmf.jar
%config(noreplace) %{_sysconfdir}/%{name}.d/jmf

%files swing -f .mfiles-swing
%{ant_home}/lib/%{name}-swing.jar
%config(noreplace) %{_sysconfdir}/%{name}.d/swing

%files antlr -f .mfiles-antlr
%{ant_home}/lib/%{name}-antlr.jar
%config(noreplace) %{_sysconfdir}/%{name}.d/antlr

%files apache-bsf -f .mfiles-apache-bsf
%{ant_home}/lib/%{name}-apache-bsf.jar
%config(noreplace) %{_sysconfdir}/%{name}.d/apache-bsf

%files apache-resolver -f .mfiles-apache-resolver
%{ant_home}/lib/%{name}-apache-resolver.jar
%config(noreplace) %{_sysconfdir}/%{name}.d/apache-resolver

%files commons-logging -f .mfiles-commons-logging
%{ant_home}/lib/%{name}-commons-logging.jar
%config(noreplace) %{_sysconfdir}/%{name}.d/commons-logging

%files commons-net -f .mfiles-commons-net
%{ant_home}/lib/%{name}-commons-net.jar
%config(noreplace) %{_sysconfdir}/%{name}.d/commons-net

%files apache-bcel -f .mfiles-apache-bcel
%{ant_home}/lib/%{name}-apache-bcel.jar
%config(noreplace) %{_sysconfdir}/%{name}.d/apache-bcel

%files apache-log4j -f .mfiles-apache-log4j
%{ant_home}/lib/%{name}-apache-log4j.jar
%config(noreplace) %{_sysconfdir}/%{name}.d/apache-log4j

%files apache-oro -f .mfiles-apache-oro
%{ant_home}/lib/%{name}-apache-oro.jar
%config(noreplace) %{_sysconfdir}/%{name}.d/apache-oro
%{ant_home}/etc/maudit-frames.xsl

%files apache-regexp -f .mfiles-apache-regexp
%{ant_home}/lib/%{name}-apache-regexp.jar
%config(noreplace) %{_sysconfdir}/%{name}.d/apache-regexp

%files apache-xalan2 -f .mfiles-apache-xalan2
%{ant_home}/lib/%{name}-apache-xalan2.jar
%config(noreplace) %{_sysconfdir}/%{name}.d/apache-xalan2

%files imageio -f .mfiles-imageio
%{ant_home}/lib/%{name}-imageio.jar
%config(noreplace) %{_sysconfdir}/%{name}.d/imageio

%files javamail -f .mfiles-javamail
%{ant_home}/lib/%{name}-javamail.jar
%config(noreplace) %{_sysconfdir}/%{name}.d/javamail

%files jdepend -f .mfiles-jdepend
%{ant_home}/lib/%{name}-jdepend.jar
%config(noreplace) %{_sysconfdir}/%{name}.d/jdepend
%{ant_home}/etc/jdepend.xsl
%{ant_home}/etc/jdepend-frames.xsl

%files jsch -f .mfiles-jsch
%{ant_home}/lib/%{name}-jsch.jar
%config(noreplace) %{_sysconfdir}/%{name}.d/jsch

%files junit -f .mfiles-junit
%{ant_home}/lib/%{name}-junit.jar
%{ant_home}/lib/%{name}-junit4.jar
%config(noreplace) %{_sysconfdir}/%{name}.d/junit
%config(noreplace) %{_sysconfdir}/%{name}.d/junit4
%{ant_home}/etc/junit-frames.xsl
%{ant_home}/etc/junit-noframes.xsl
%{ant_home}/etc/junit-frames-xalan1.xsl
%{ant_home}/etc/junit-frames-saxon.xsl
%{ant_home}/etc/junit-noframes-saxon.xsl

%files junit5 -f .mfiles-junitlauncher
%{ant_home}/lib/%{name}-junitlauncher.jar
%config(noreplace) %{_sysconfdir}/%{name}.d/junitlauncher

%files testutil -f .mfiles-testutil
%{ant_home}/lib/%{name}-testutil.jar
%config(noreplace) %{_sysconfdir}/%{name}.d/testutil

%files xz -f .mfiles-xz
%{ant_home}/lib/%{name}-xz.jar
%config(noreplace) %{_sysconfdir}/%{name}.d/xz

%files help
%license LICENSE NOTICE
%doc KEYS README WHATSNEW
%doc manual/*
%{_javadocdir}/%{name}

%changelog
* Mon Dec 02 2024 zhaosaisai <zhaosaisai@kylinos.cn> - 0:1.10.12-2
- Add Fix-integer-overflow-when-parsing-SOURCE_DATE_EPOCH

* Fri Jan 21 2022 SimpleUpdate Robot <tc@openeuler.org> - 1.10.12-1
- Upgrade to version 1.10.12

* Mon Jul 19 2021 yaoxin <yaoxin30@huawei.com> - 0:1.10.8-4
- Fix CVE-2021-36373 CVE-2021-36374

* Mon Nov 30 2020 huanghaitao <huanghaitao8@huawei.com> - 0:1.10.8-3
- Fix CVE-2020-11979

* Thu Oct 15 2020 lingsheng<lingsheng@huawei.com> - 0:1.10.8-2
- Change buildrequire and require to java-1.8.0-devel

* Wed Sep 9 2020 zhanghua<zhanghua40@huawei.com> - 0:1.10.8-1
- update to 1.10.8

* Sat Mar 14 2020 zhujunhao<zhujunhao5@huawei.com> - 0:1.10.5-7
- Split ant pack into subpackets

* Fri Feb 14 2020 gulining<gulining1@huawei.com> - 0:1.10.5-6
- Package init
